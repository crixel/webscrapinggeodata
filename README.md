# WebScrapingGeoData

This project is a brief example of how to scrape some geographical data from a (quite specific) web site using BeautifulSoup and regular expressions (and some other frameworks).

## General info
The motivation for this project came from the need to gather numerical (geographic coordinates) and textual (description of coordinates) information from a specific tourist information site. The task comes from an assignment given to my flat mate. I personally view it as some sort of 'memo to self' on how to perform web scraping and handle regular expressions. 

And since the information retrieval made by hand can be the source of many severe mistakes and can lead to the failure of an assignment, I decided to write a Jupyter Notebook to help scraping the needed information from the given site automatically. It is to say though, that the site is quite specific (containing some format errors, for example) and so the notebook offers only a specific and less general solution to the task. Thereby it is possible at some point, to implement a more general approach for the solution.

## Technologies / Frameworks
* requests - version 2.23.0
* BeautifulSoup - version 4.6.0
* re - version 2.2.1
* folium - version 0.11.0
* csv - version 1.0

## Setup
In order to be able to execute this Jupyter Notebook it will be of course necessary to have a version of Jupyter Notebook or Jupyter Lab installed. In most cases the above mentioned frameworks will have to be installed as well. See the according notebook cells for further information. 

## Contact
Feel free to contact me here over GitLab!